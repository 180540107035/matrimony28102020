package com.example.matrimony.Adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.matrimony.Model.CityModel;

import java.util.ArrayList;

public class CityAdapter extends BaseAdapter {

    ArrayList<CityModel> cityList;
    Activity context;

    public CityAdapter(ArrayList<CityModel> cityList, Activity context) {
        this.cityList = cityList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return cityList.size();
    }

    @Override
    public Object getItem(int i) {
        return cityList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return cityList.get(i).getCityId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
