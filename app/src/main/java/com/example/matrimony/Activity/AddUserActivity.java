package com.example.matrimony.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.matrimony.Database.TblCandidate;
import com.example.matrimony.Model.CandidateModel;
import com.example.matrimony.R;

import java.util.ArrayList;

public class AddUserActivity extends AppCompatActivity {

    EditText adduser_edittext_name;
    EditText adduser_edittext_fathername;
    EditText adduser_edittext_surname;
    EditText adduser_edittext_mobile;
    EditText adduser_edittext_email;
    EditText adduser_edittext_dob;

    RadioButton adduser_rb_male,adduser_rb_female;

    RadioGroup adduser_rg_gender;

    CheckBox adduser_ch_english;
    CheckBox adduser_ch_gujarati;
    CheckBox adduser_ch_hindi;
    CheckBox adduser_ch_reading;
    CheckBox adduser_ch_travelling;
    CheckBox adduser_ch_cooking;

    Button adduser_btn_submit;

    //Temp Variables
    String name,fathername,surname,email,mobile,dob,hobbies,languages;
    boolean rbMale;

    long insertedCheck;

    ArrayList<String> checkBoxSelectedHobbies;
    ArrayList<String> checkBoxSelectedLanguages;

    CandidateModel candidateModel;

    TblCandidate tblCandidate;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_user);

        init();
        process();
        listener();
    }

    void init(){

        adduser_edittext_name = findViewById(R.id.adduser_edittext_name);
        adduser_edittext_fathername  = findViewById(R.id.adduser_edittext_fathername);
        adduser_edittext_surname = findViewById(R.id.adduser_edittext_surname);
        adduser_edittext_mobile = findViewById(R.id.adduser_edittext_mobile);
        adduser_edittext_email = findViewById(R.id.adduser_edittext_email);
        adduser_edittext_dob = findViewById(R.id.adduser_edittext_dob);

        adduser_rb_male = findViewById(R.id.adduser_rb_male);
        adduser_rb_female = findViewById(R.id.adduser_rb_female);
        adduser_rg_gender = findViewById(R.id.adduser_rg_gender);

        adduser_ch_english = findViewById(R.id.adduser_ch_english);
        adduser_ch_gujarati = findViewById(R.id.adduser_ch_gujarati);
        adduser_ch_hindi = findViewById(R.id.adduser_ch_hindi);
        adduser_ch_reading = findViewById(R.id.adduser_ch_reading);
        adduser_ch_travelling = findViewById(R.id.adduser_ch_travelling);
        adduser_ch_cooking = findViewById(R.id.adduser_ch_cooking);

        adduser_btn_submit = findViewById(R.id.adduser_btn_submmit);

        candidateModel = new CandidateModel();

        tblCandidate= new TblCandidate(this);

        checkBoxSelectedHobbies=new ArrayList<>();
        checkBoxSelectedLanguages=new ArrayList<>();


    }
    void process(){

    }


    private void listener() {

        adduser_btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDataFromFields();

                if(isValid())
                {
                    candidateModel.setName(name);
                    candidateModel.setFathername(fathername);
                    candidateModel.setSurname(surname);
                    candidateModel.setEmail(email);
                    //candidateModel.setGender();
                    candidateModel.setMobile(mobile);
                    candidateModel.setHobbies(hobbies);
                    candidateModel.setLanguages(languages);
                    candidateModel.setIsFavourite(0);
                    //candidateModel.setCityId();

                    insertedCheck = tblCandidate.insertedCheck(candidateModel);

                    if(insertedCheck>0){
                        Toast.makeText(AddUserActivity.this, "SAVED", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Toast.makeText(AddUserActivity.this, "Something went Wrong", Toast.LENGTH_SHORT).show();
                    }

                }
            }
        });
    }

    public boolean isValid(){

        getDataFromFields();

        boolean valid = true;

        if(TextUtils.isEmpty(name)){
            adduser_edittext_name.setError("Enter Some Details");
            adduser_edittext_name.requestFocus();
            valid=false;
        }
        if(TextUtils.isEmpty(fathername)){
            adduser_edittext_fathername.setError("Enter Some Details");
            adduser_edittext_fathername.requestFocus();
            valid=false;
        }
        if(TextUtils.isEmpty(surname)){
            adduser_edittext_surname.setError("Enter Some Details");
            adduser_edittext_surname.requestFocus();
            valid=false;
        }
        if(TextUtils.isEmpty(email)){
            adduser_edittext_email.setError("Enter Some Details");
            adduser_edittext_email.requestFocus();
            valid=false;
        }
        if(TextUtils.isEmpty(mobile)){
            adduser_edittext_mobile.setError("Enter Some Details");
            adduser_edittext_mobile.requestFocus();
            valid=false;
        }


        return valid;
    }

    void getDataFromFields(){

        name=dataSpaceRemove(adduser_edittext_name.getText().toString());
        fathername=dataSpaceRemove(adduser_edittext_fathername.getText().toString());
        surname=dataSpaceRemove(adduser_edittext_surname.getText().toString());
        email=dataSpaceRemove(adduser_edittext_email.getText().toString());
        mobile=dataSpaceRemove(adduser_edittext_mobile.getText().toString());
        dob=dataSpaceRemove(adduser_edittext_dob.getText().toString());


        rbMale = adduser_rb_male.isChecked();

        hobbies = " ";
        checkBoxSelectedHobbies = new ArrayList<>();
        if(adduser_ch_reading.isChecked()){
            checkBoxSelectedHobbies.add("Reading");
        }
        if(adduser_ch_travelling.isChecked()){
            checkBoxSelectedHobbies.add("Travelling");
        }
        if(adduser_ch_cooking.isChecked()){
            checkBoxSelectedHobbies.add("Cooking");
        }
        int i=0;
        for(i=0;i<checkBoxSelectedHobbies.size();i++){
            if(checkBoxSelectedHobbies.size()>1){
                hobbies += ", " + checkBoxSelectedHobbies.get(i);
            }else{
                hobbies += checkBoxSelectedHobbies.get(i);
            }
        }

        languages = " ";
        checkBoxSelectedLanguages = new ArrayList<>();
        if(adduser_ch_english.isChecked()){
            checkBoxSelectedLanguages.add("English");
        }
        if(adduser_ch_gujarati.isChecked()){
            checkBoxSelectedLanguages.add("Gujarati");
        }
        if(adduser_ch_hindi.isChecked()){
            checkBoxSelectedLanguages.add("Hindi");
        }
        int j=0;
        for(j=0;i<checkBoxSelectedLanguages.size();j++){
            if(checkBoxSelectedLanguages.size()>1){
                hobbies += ", " + checkBoxSelectedLanguages.get(j);
            }else{
                hobbies += checkBoxSelectedLanguages.get(j);
            }
        }



    }

    String dataSpaceRemove(String s){

        s=s.trim();
        return s;
    }
}