package com.example.matrimony.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.matrimony.R;

public class MainActivity extends AppCompatActivity {

    TextView main_activity_register;
    TextView main_activity_display;
    TextView main_activity_favourite;
    TextView main_activity_search;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        listeners();
    }
    void init(){

        main_activity_register = findViewById(R.id.main_activity_register);
        main_activity_display = findViewById(R.id.main_activity_display);
        main_activity_favourite = findViewById(R.id.main_activity_favourite);
        main_activity_search = findViewById(R.id.main_activity_search);
    }
    void listeners(){

        main_activity_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,AddUserActivity.class);
                startActivity(intent);
            }
        });

    }
}
