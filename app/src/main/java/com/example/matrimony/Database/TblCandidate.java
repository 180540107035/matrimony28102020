package com.example.matrimony.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony.Model.CandidateModel;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class TblCandidate extends DatabaseHelper {

    public static String TBL_NAME ="TblCandidate";
    public static String COL_REGID = "RegId";
    public static String COL_NAME = "Name";
    public static String COL_FATHERNAME = "Fathername";
    public static String COL_SURNAME = "Surname";
    public static String COL_EMAIL = "Email";
    public static String COL_MOBILE = "Mobile";
    public static String COL_DOB = "DOB";
    public static String COL_GENDER = "Gender";
    public static String COL_HOBBIES = "HObbies";
    public static String COL_LANGUAGES = "Languages";
    public static String COL_ISFAVOURITE = "IsFavourite";
    public static String COL_CITYNAME= "CityName";
    public static String COl_CITYID = "CityId";




    public TblCandidate(Context context) {
        super(context);
    }

    public long insertedCheck(CandidateModel candidateModel){

        SQLiteDatabase db = getWritableDatabase();

        ContentValues cv = new ContentValues();

        cv.put(COL_REGID,candidateModel.getRegId());
        cv.put(COL_NAME,candidateModel.getName());
        cv.put(COL_FATHERNAME,candidateModel.getFathername());
        cv.put(COL_SURNAME,candidateModel.getSurname());
        cv.put(COL_EMAIL,candidateModel.getEmail());
        cv.put(COL_DOB,candidateModel.getDOB());
        cv.put(COL_MOBILE,candidateModel.getMobile());
        cv.put(COL_GENDER,candidateModel.getGender());
        cv.put(COL_HOBBIES,candidateModel.getHobbies());
        cv.put(COL_LANGUAGES,candidateModel.getLanguages());
        cv.put(COl_CITYID,candidateModel.getCityId());
        cv.put(COL_ISFAVOURITE,candidateModel.getIsFavourite());

        long insertedCheck = db.insert(TBL_NAME,null,cv);

        return insertedCheck;
    }
}
