package com.example.matrimony.Model;

import java.io.Serializable;

public class CityModel implements Serializable {

    int CityId;
    String CityName;

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    @Override
    public String toString() {
        return "CityModel{" +
                "CityId=" + CityId +
                ", CityName='" + CityName + '\'' +
                '}';
    }
}
