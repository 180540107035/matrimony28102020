package com.example.matrimony.Model;

import java.io.Serializable;

public class CandidateModel implements Serializable {

    int RegId;
    String Name;
    String Fathername;
    String Surname;
    String Email;
    String Mobile;
    int Gender;
    String Hobbies;
    String Languages;
    String CityId;
    String DOB;
    int IsFavourite;



    public CandidateModel() {

    }


    public int getRegId() {
        return RegId;
    }

    public void setRegId(int regId) {
        RegId = regId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFathername() {
        return Fathername;
    }

    public void setFathername(String fathername) {
        Fathername = fathername;
    }

    public String getSurname() {
        return Surname;
    }

    public void setSurname(String surname) {
        Surname = surname;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public int getGender() {
        return Gender;
    }

    public void setGender(int gender) {
        Gender = gender;
    }

    public String getHobbies() {
        return Hobbies;
    }

    public void setHobbies(String hobbies) {
        Hobbies = hobbies;
    }

    public String getLanguages() {
        return Languages;
    }

    public void setLanguages(String languages) {
        Languages = languages;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public int getIsFavourite() {
        return IsFavourite;
    }

    public void setIsFavourite(int isFavourite) {
        IsFavourite = isFavourite;
    }

    @Override
    public String toString() {
        return "CandidateModel{" +
                "RegId=" + RegId +
                ", Name='" + Name + '\'' +
                ", Fathername='" + Fathername + '\'' +
                ", Surname='" + Surname + '\'' +
                ", Email='" + Email + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", Gender=" + Gender +
                ", Hobbies='" + Hobbies + '\'' +
                ", Languages='" + Languages + '\'' +
                ", CityId='" + CityId + '\'' +
                ", DOB='" + DOB + '\'' +
                ", IsFavourite=" + IsFavourite +
                '}';
    }
}
